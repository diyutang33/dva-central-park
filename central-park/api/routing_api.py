# This file contains 3 functions and a test case. The objective
# of this file is to return a dataframe containing the total walking
# distance, total walking time, total driving distance, and total driving
# time for each parking hexagon. This work is part of the Central Park App
# workflow pipeline.

# The 2 key inputs are the dict_POI and dict_ParkHex. The dict_POI is a
# dictionary of dictionaries containing POI data while the dict_ParkHex
# contains parking hexagon data. Please see the test case below for formatting
# requirements.

# The high-level workflow involves calling the 3 functions in the order they
# appear in this file - get_walk_dist_time(), get_drive_dist_time(),
# and get_df_output() - passing the output of one function as an input
# into the next function. The parking hexagon dictionary is altered
# throughout this process, culminating in its final form as a pandas dataframe.

# More specifically, the get_walk_dist_time() function involves calling the
# Bing Routing API to get the total walking distance and total walking time
# for each parking hexagon while the get_drive_dist_time() function gets the
# total driving distance and time. The get_df_output() function transforms
# the changed parking hexagon dictionary into a nice looking dataframe.

import os
import json
import requests
import pandas as pd
from typing import List
from copy import deepcopy

api_key = os.environ.get("BING_ROUTES_API_KEY")


def get_walk_dist_time_to_first_dest(poi_list: List, park_hex_list: List):
    """
    :param dict_POI: This is a dictionary of dictionaries that contains
    POI's (points of interest) that are inputted by the user, in the correct
    order of planned POI visitation (Important!!!; order marked by the key
    of the highest level of the dictionary of dictionaries). Each nested
    dictionary refers to a different POI and contains the following keys -
    "name" (POI name), "lat" (POI latitude), "lng" (POI longitude).

    :param dict_ParkHex: This is a dictionary of dictionaries that contains
    ParkHex's (parking hexagons), that are outputted (and filtered) by the
    HexCNN algorithm. Each nested dictionary refers to a different parking
    hexagon and contains the following keys - "Name" (parking hexagon name/ID),
    "lat" (parking hexagon latitude), "lng" (parking hexagon longitude).

    :return: This function returns a dictionary of dictionaries containing
    the latitude, longitude, total walking distance, and total walking time
    for each parking hexagon.
    """

    # ***Get walking distance and walking time between each Parking Hex and
    # first POI:

    # This for loop gets the lat's and long's in the right format to pass
    # into the API:
    new_park_hex_list = deepcopy(park_hex_list)
    for new_park_hex in new_park_hex_list:
        route_parkHex_to_first_POI = (f'{new_park_hex["lat"]},'
                                      f'{new_park_hex["lng"]}&wp.1='
                                      f'{poi_list[0]["lat"]},'
                                      f'{poi_list[0]["lng"]}')

        # Calls the API:
        url = 'https://dev.virtualearth.net/REST/v1/Routes/Walking?'
        r = requests.get(url + 'wp.0=' + route_parkHex_to_first_POI
                         + "&distanceUnit=mi"
                         + "&durationUnit=min"
                         + '&key=' + api_key)
        x = r.json()
        # Extracts the walking distance and time from the API output:
        route_output_walk_dist = x["resourceSets"][0]["resources"][0][
            "travelDistance"]
        route_output_walk_time = (x["resourceSets"][0]["resources"][0][
            "travelDuration"]) / 60

        # Final massaging to get walking distance and time into the park
        # hexagon dictionary
        new_park_hex['Total Walking Distance (miles)'] = \
            round(route_output_walk_dist, 1)
        new_park_hex['Total Walking Time (minutes)'] = \
            round(route_output_walk_time, 1)

    return new_park_hex_list


# ***Get total walking distance and time from each parking hexagon to all
# destinations in order and back to the parking hexagon:
def get_total_walk_dist_time(poi_list: List, park_hex_list: List):
    new_park_hex_list = deepcopy(park_hex_list)
    route_btw_POIs = ""
    # Building Bing routes API endpoint
    for count, poi in enumerate(poi_list):
        route_btw_POIs += f'{poi["lat"]},{poi["lng"]}'
        if count < len(poi_list) - 1:
            route_btw_POIs += f'&wp.{count + 1}='
    print(f'url: {route_btw_POIs}')
    # Calls the API:
    url = 'https://dev.virtualearth.net/REST/v1/Routes/Walking?'
    r = requests.get(url + 'wp.0=' + route_btw_POIs +
                     "&distanceUnit=mi" +
                     "&durationUnit=min" +
                     '&key=' + api_key)
    x = r.json()

    # Extracts the walking distance and time from the API output:
    route_output_walk_dist = x["resourceSets"][0]["resources"][0][
        "travelDistance"]
    route_output_walk_time = (x["resourceSets"][0]["resources"][0][
        "travelDuration"]) / 60

    # Final massaging to get walking distance and time into the park hexagon
    # dictionary
    for new_park_hex in new_park_hex_list:
        new_park_hex['total_walk_dist'] = route_output_walk_dist
        new_park_hex['total_walk_time'] = route_output_walk_time

    # ***Get walking distance and time between each Parking Hex and last POI
    last_poi = poi_list[-1]
    # Building Bing routes API endpoint
    for new_park_hex in new_park_hex_list:
        route_last_POI_to_park_hex = (f'{last_poi["lat"]},'
                                      f'{last_poi["lng"]}&wp.1='
                                      f'{new_park_hex["lat"]},'
                                      f'{new_park_hex["lng"]}')

        # Calls the API:
        url = 'https://dev.virtualearth.net/REST/v1/Routes/Walking?'
        r = requests.get(url + 'wp.0=' + route_last_POI_to_park_hex
                         + "&distanceUnit=mi"
                         + "&durationUnit=min"
                         + '&key=' + api_key)
        x = r.json()

        # Extracts the walking distance and time from the API output:
        route_output_walk_dist = x["resourceSets"][0]["resources"][0][
            "travelDistance"]
        route_output_walk_time = (x["resourceSets"][0]["resources"][0][
            "travelDuration"]) / 60

        # Final massaging to get walking distance and time into the park
        # hexagon dictionary
        new_park_hex['total_walk_dist'] = round(
            new_park_hex['total_walk_dist'] + route_output_walk_dist, 1)
        new_park_hex['total_walk_time'] = round(
            new_park_hex['total_walk_time'] + route_output_walk_time, 1)

    return new_park_hex_list


def get_drive_dist_time(user_loc, park_hex_list):
    """
    :param user_loc: This is a string containing the latitude and longitude
    of the user's location/starting point, inputted by the user. E.g.
    "37.774031, -122.463805".

    :param dict_ParkHex_altered: This is a dictionary of dictionaries that
    is returned by the get_walk_dist_time() function. It contains the
    latitude, longitude, total walking distance, and total walking time for
    each parking hexagon.

    :return: This function returns a dictionary of dictionaries containing
    the latitude, longitude, total walking distance, total walking time,
    total driving distance, and total driving time for each parking hexagon.
    """

    # ***Get driving distance and driving time between user location and
    # each Parking Hex:

    # This for loop gets the lat's and long's in the right format to pass
    # into the API:
    new_park_hex_list = deepcopy(park_hex_list)
    for new_park_hex in new_park_hex_list:
        route_userloc_to_park_hex = (f'{user_loc}&wp.1='
                                     f'{new_park_hex["lat"]},'
                                     f'{new_park_hex["lng"]}')

        # Calls the API:
        url = 'https://dev.virtualearth.net/REST/v1/Routes/Driving?'
        r = requests.get(url + 'wp.0=' + route_userloc_to_park_hex
                         + "&distanceUnit=mi"
                         + "&durationUnit=min"
                         + '&key=' + api_key)

        x = r.json()

        # Extracts the driving distance and time from the API output:
        route_output_drive_dist = x["resourceSets"][0]["resources"][0][
            "travelDistance"]
        route_output_drive_time = (x["resourceSets"][0]["resources"][0][
            "travelDurationTraffic"]) / 60

        # Final massaging to get driving distance and time into the park
        # hexagon dictionary
        new_park_hex['Total Driving Distance (miles)'] = \
            round(route_output_drive_dist, 1)
        new_park_hex['Total Driving Time (minutes)'] = \
            round(route_output_drive_time, 1)

    return new_park_hex_list


def get_df_output(park_hex_list):
    """
    :param dict_ParkHex_final: This is a dictionary of dictionaries that is
    returned by the get_drive_dist_time() function. It contains the
    latitude, longitude, total walking distance, total walking time,
    total driving distance, and total driving time for each parking hexagon.

    :return: This function converts the input into a table, returning a
    nice looking pandas dataframe.
    """
    df = pd.DataFrame(park_hex_list)

    # Citation for print_full() function:
    # https://stackoverflow.com/questions/25351968/how-can-i-display-full
    # -non-truncated-dataframe-information-in-html-when-conver
    def print_full(x):
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.width', 2000)
        # pd.set_option('display.float_format', '{:20,.2f}'.format)
        pd.set_option('display.max_colwidth', None)
        print(x)
        pd.reset_option('display.max_rows')
        pd.reset_option('display.max_columns')
        pd.reset_option('display.width')
        pd.reset_option('display.float_format')
        pd.reset_option('display.max_colwidth')

    return print_full(df)


# ********************************TEST CASE:************************************

# Dictionary of dictionaries containing POI's:
poi_list = [
    {
        "name": "PlenTea",
        "lat": 37.7913486,
        "lng": -122.4043108
    },
    {
        "name": "i-Tea",
        "lat": 37.7903534,
        "lng": -122.4041188
    },
    {
        "name": "Boba Guys",
        "lat": 37.7900012,
        "lng": -122.4072208
    },
    {
        "name": "Gong Cha",
        "lat": 37.7864688,
        "lng": -122.4092476
    }
]

# Dictionary of dictionaries containing Parking Hex's:
park_hex_list = [
    {
        "name": "ParkHex_1",
        "lat": 37.788392,
        "lng": -122.40775
    },
    {
        "name": "ParkHex_2",
        "lat": 37.789507,
        "lng": -122.403743
    }
]

# User's location or starting point coordinates:
user_loc = "37.774031,-122.463805"

# Calls the 3 functions (in order) to get the final pretty df:
# poi_to_hex_walk_dist_time = get_walk_dist_time(poi_list, park_hex_list)
# get_df_output(poi_to_hex_walk_dist_time)
# user_loc_to_hex_drive_dist_time = get_drive_dist_time(user_loc, park_hex_list)
# get_df_output(user_loc_to_hex_drive_dist_time)
# print(get_total_walk_dist_time(poi_list, park_hex_list))
