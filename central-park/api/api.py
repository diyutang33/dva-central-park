import time
import copy
from flask import Flask, request
from routing_api import get_walk_dist_time_to_first_dest, \
    get_total_walk_dist_time, get_drive_dist_time
from hex_cnn import hex_cnn
from mock_result import mock_result
app = Flask(__name__)


@app.route('/api/hexcnn', methods=['get'])
def tester():
    return {"text":"it works!"}


@app.route('/api/hexcnn', methods=['POST'])
def hexcnn():
    timer = time.time()
    data = request.json
    hex_dict = {}
    origin = data["addressList"]["origin"]
    dest_list = data["addressList"]["dest"]
    if data["source"] == "/mock":
        hex_dict = mock_result()
    elif data["source"] == "/" or  data["source"] == "/demo":
        hex_dict = hex_cnn(dest_list[0]["address"])
    hex_list = hex_dict_to_list(hex_dict)
    print("Calculating total walk distance and time between all hex to first dest.")
    hex_list = calc_dist_time(origin, dest_list, hex_list)
    print(time.time() - timer)
    print("Calculating walk distance and time between all dest.")
    hex_list = get_total_walk_dist_time(dest_list, hex_list)
    print(time.time() - timer)

    response = {
        "origin": origin,
        "dest": dest_list,
        "hex": hex_list,
        "maxWalkTime": data["maxWalkTime"]
    }
    return response


def hex_dict_to_list(result):
    new_result = []
    for k in result["value"].keys():
        prob = result["value"][k]
        if not isinstance(prob, float):
            prob = 0.0
        new_result.append({
            "value": prob,
            "lat": result["Latitude"][k],
            "lng": result["Longitude"][k],
            "hexEdge": result["hex_edge"][k]})
    return new_result

def calc_dist_time(origin, dest_list, hex_list):
    hex_walk_dist_time = get_walk_dist_time_to_first_dest(dest_list, hex_list)
    origin_lat_lng = f'{origin["lat"]},{origin["lng"]}'
    hex_drive_dist_time = get_drive_dist_time(origin_lat_lng, hex_list)
    new_hex_list = copy.deepcopy(hex_list)
    for i, hex in enumerate(hex_walk_dist_time):
        new_hex_list[i]["walk_dist"] = hex["Total Walking Distance (miles)"]
        new_hex_list[i]["walk_time"] = hex["Total Walking Time (minutes)"]
    for i, hex in enumerate(hex_drive_dist_time):
        new_hex_list[i]["drive_dist"] = hex["Total Driving Distance (miles)"]
        new_hex_list[i]["drive_time"] = hex["Total Driving Time (minutes)"]



    return new_hex_list

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
