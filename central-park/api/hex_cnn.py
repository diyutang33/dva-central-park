import os
import time
import argparse
import numpy as np
import pandas as pd

pd.options.mode.chained_assignment = None
from datetime import datetime, timedelta
import pickle5 as pickle
import json
from pygeocoder import Geocoder
import h3
import tensorflow as tf
from sodapy import Socrata
import keras

def roundStartTime(dt, minutes=15):
    """
        Round off given timestamp to given minute interval.
        
        Input - dt (datetime)
                minutes (int)
                
        Output - dt (datetime)
    """
    dt_minutes = dt.minute % 15
    dt_sec = dt.second

    return dt - timedelta(minutes=dt_minutes, seconds=dt_sec)


def roundEndTime(dt, minutes=15):
    """ 
        Round up given timestamp to given minute interval.
        
        Input - dt (datetime)
                minutes (int)
                
        Output - dt (datetime)
    """
    dt_minutes = 15 - (dt.minute % 15) - 1
    dt_sec = 60 - dt.second

    return dt + timedelta(minutes=dt_minutes, seconds=dt_sec)


def get_all_hex_id(gpd, res):
    """
        Get all unique h3 hex_id for SF
        
        Input - gpd  (Dataframe) Dataframe containing geojson information
                res (int) resolution
        Output - list of h3 hex_id covering all of Chicago
    """

    all_hex_id = []

    for i in range(len(gpd)):
        set_hexagons = h3.polyfill(geojson=gpd['geom_swap_geojson'][i],
                                   res=res)
        list_hex = list(set_hexagons)
        all_hex_id.append(list_hex)

    all_hex_id = sum(all_hex_id, [])

    return all_hex_id


def fill_h3_ids(pivoted_df, all_hex_id):
    """
        Fill with non-existed hex_ids and concatnate to the dataframe
        
        Input - pivoted_df (DataFrame) Dataframe containing 
    """
    non_existed = {}
    nrow = pivoted_df.shape[0]
    for hex_id in all_hex_id:
        if hex_id not in pivoted_df.columns:
            non_existed[hex_id] = np.zeros(nrow)

    new_df = pd.DataFrame(non_existed)
    new_df.index = pivoted_df.index

    final = pd.concat([pivoted_df, new_df], axis=1)

    return final


def h3_df_to_matrix(df, valuename="parking_occupancy"):
    """
        Given a DataFrame containing timestamp, hex_id and pickup value, convert to matrix/ 2d array
        
        Input - df (DataFrame) with hex_ids and values

        Output - 2d array based on hex_id ij coordinates with value speicified
        
    """
    dict_ij = {}  # location i,j
    dict_values = {}  # Pick up values

    local_origin = df.iloc[0]['hex_id']
    for i, row in df.iterrows():
        ij_ex = h3.experimental_h3_to_local_ij(origin=local_origin,
                                               h=row["hex_id"])
        dict_ij[row["hex_id"]] = ij_ex
        dict_values[row["hex_id"]] = row[valuename]

    # post-process
    min_i = min([dict_ij[h][0] for h in dict_ij])
    min_j = min([dict_ij[h][1] for h in dict_ij])

    max_i = max([dict_ij[h][0] for h in dict_ij])
    max_j = max([dict_ij[h][1] for h in dict_ij])

    # rescale
    dict_ij_rescaled = {}
    for h in dict_ij:
        dict_ij_rescaled[h] = [dict_ij[h][0] - min_i, dict_ij[h][1] - min_j]

    num_rows = max_i - min_i + 1
    num_cols = max_j - min_j + 1

    arr_ij = np.zeros(shape=(num_rows, num_cols), dtype=np.float32)

    for h in dict_ij_rescaled:
        arr_ij[dict_ij_rescaled[h][0]][dict_ij_rescaled[h][1]] = dict_values[h]
    # arr_ij = np.rot90(arr_ij,2)
    return arr_ij


def h3_df_to_matrix_hex_id(df):
    """
        Given a DataFrame containing hex_ids with number of meters, 
        convert to 2d array with hex_ids
    """
    dict_ij = {}  # location i,j
    dict_values = {}  # Pick up values

    local_origin = df.iloc[0]['hex_id']
    for i, row in df.iterrows():
        ij_ex = h3.experimental_h3_to_local_ij(origin=local_origin,
                                               h=row["hex_id"])
        dict_ij[row["hex_id"]] = ij_ex  # Store Hex Id in ij_ex

    # post-process
    min_i = min([dict_ij[h][0] for h in dict_ij])
    min_j = min([dict_ij[h][1] for h in dict_ij])

    max_i = max([dict_ij[h][0] for h in dict_ij])
    max_j = max([dict_ij[h][1] for h in dict_ij])

    # rescale
    dict_ij_rescaled = {}
    for h in dict_ij:
        dict_ij_rescaled[h] = [dict_ij[h][0] - min_i, dict_ij[h][1] - min_j]

    num_rows = max_i - min_i + 1
    num_cols = max_j - min_j + 1

    arr_ij = np.zeros(shape=(num_rows, num_cols), dtype=np.float32)
    arr_ij = arr_ij.astype(str)

    for h in dict_ij_rescaled:
        arr_ij[dict_ij_rescaled[h][0]][dict_ij_rescaled[h][1]] = h

    return arr_ij


def estimate_empty_parking_prob(test_h3, h3_total_cap):
    new_test_h3 = np.squeeze(test_h3)
    prob_h3 = new_test_h3 / h3_total_cap

    prob_h3[np.isnan(prob_h3)] = -1
    new_prob_h3 = 1 - prob_h3
    updated_prob_h3 = np.where(new_prob_h3 >= 2, 0, new_prob_h3)
    return updated_prob_h3


def filling_parking(unique_id, sf_data, day_range, rounded_user_time):
    """
    Given DataFrame and unique location id,
    Calculate the total number of parking occupancy based on day_range
    
    Input - unique_id (list)
            sf_data (DataFrame)
            day_range (DatetimeIndex)
                
    Output - unique_id_dict (dict)
    """
    unique_id_dict = {}
    for u_id in unique_id:
        day_dict = dict(zip(day_range, np.zeros(len(day_range))))
        df_post_id = sf_data[sf_data['hex_id'] == u_id]

        for start_time, end_time in zip(df_post_id['start_rounded_time'],
                                        df_post_id['end_rounded_time']):
            if end_time >= rounded_user_time:
                occupied_filter = (day_range >= start_time) & (
                            day_range <= rounded_user_time)
            else:
                occupied_filter = (day_range >= start_time) & (
                            day_range <= end_time)

            occupied_timeframe = day_range[occupied_filter]

            for timeframe in occupied_timeframe:
                day_dict[timeframe] = day_dict.get(timeframe, 0) + 1

        unique_id_dict[u_id] = pd.Series(day_dict)

    return unique_id_dict


def change_timeformat(df):
    df['start_rounded_time'] = df['session_start_dt'].apply(
        lambda x: roundStartTime(x))
    df['end_rounded_time'] = df['session_end_dt'].apply(
        lambda x: roundEndTime(x))
    df['start_timestamp'] = df['start_rounded_time'].apply(
        lambda x: x.strftime("%H:%M:%S"))
    df['end_timestamp'] = df['end_rounded_time'].apply(
        lambda x: x.strftime("%H:%M:%S"))

    return df


def run_model_with_api(user_lat, user_lgn, str_user_time):
    # Getting user requested time and location
    user_time = datetime.strptime(str_user_time, "%Y-%m-%d %H:%M:%S")
    user_weekday = user_time.weekday()
    rounded_user_time = roundStartTime(user_time)

    loc_h3 = h3.geo_to_h3(lat=user_lat, lng=user_lgn, resolution=9)
    loc_h3_ring = h3.h3.k_ring(loc_h3, 2)

    # Getting all necessary data
    with open(r"pickle_data/hex_9_total_meters.pickle", "rb") as input_file:
        all_sf_meter_h3 = pickle.load(input_file)

    with open(r"pickle_data/post_id_to_hex_id.pickle", "rb") as input_file:
        post_hex_mapping = pickle.load(input_file)

    df_all_sf_meter_h3 = pd.Series(
        all_sf_meter_h3).to_frame().reset_index().rename(
        columns={'index': 'hex_id', 0: 'Num_Meters'})
    h3_total_capacity = h3_df_to_matrix(df_all_sf_meter_h3, 'Num_Meters')
    h3_id_matrix_2d = h3_df_to_matrix_hex_id(df_all_sf_meter_h3)

    # Getting the most recent sf parking session data using API
    myapp_token = os.environ.get("SOCRATA_API_KEY")
    client = Socrata("data.sfgov.org",
                     app_token=myapp_token,
                     username=os.environ.get("SOCRATA_API_USERNAME"),
                     password=os.environ.get("SOCRATA_API_PASSWORD"))
    print("Getting Data from data.sfgov.org \n")
    timer = time.time()
    results = client.get("imvp-dq3v",
                         select='post_id, session_start_dt, session_end_dt',
                         order="session_start_dt DESC",
                         limit=500000)
    print(time.time() - timer)
    results_df = pd.DataFrame.from_records(results)
    print("Data Loading Done. Start Manipulating Data... \n")
    results_df['session_start_dt'] = pd.to_datetime(
        results_df['session_start_dt'])
    results_df['session_end_dt'] = pd.to_datetime(results_df['session_end_dt'])
    results_df['weekday'] = results_df['session_start_dt'].apply(
        lambda x: x.weekday())

    # Filtering to get the most recent data corresponds to the day of week when
    # user requests
    filtered_df = change_timeformat(
        results_df[results_df['weekday'] == user_weekday])
    filtered_df['hex_id'] = filtered_df['post_id'].map(post_hex_mapping)
    final_df = filtered_df[~filtered_df['hex_id'].isnull()]

    # Getting corresponding timeframe of input
    recent_date = max(final_df['start_rounded_time']).date()
    date_diff = rounded_user_time.date() - recent_date
    test_start_time = rounded_user_time - date_diff - timedelta(hours=2)
    test_timeframe = pd.Series(
        pd.date_range(start=test_start_time, periods=8, freq='15T'))

    condition = (final_df['start_rounded_time'].isin(test_timeframe)) | (
        final_df['end_rounded_time'].between(test_timeframe[0],
                                             test_timeframe[7]))
    test_df = final_df[condition]

    unique_hex = list(set(test_df['hex_id']))

    # Filling Parking Occupancy in the test timeframe and non-existing hex_id
    hex_id_dict = filling_parking(unique_hex, test_df, test_timeframe,
                                  rounded_user_time)
    pivoted_hex_id = pd.DataFrame(hex_id_dict)
    pivoted_hex_id = fill_h3_ids(pivoted_hex_id, list(all_sf_meter_h3.keys()))

    before_train_h3 = pivoted_hex_id.reset_index().melt('index').sort_values(
        'index').reset_index(drop=True)
    before_train_h3.rename(columns={'index': 'timestamp', 'variable': 'hex_id',
                                    'value': 'parking_occupancy'},
                           inplace=True)

    hex_chunk = before_train_h3.hex_id.nunique()
    x = []
    for j in range(8):
        x_15 = h3_df_to_matrix(
            before_train_h3[j * hex_chunk:(j + 1) * hex_chunk])
        x.append(np.array(x_15))

    test_x = np.array(x).transpose([1, 2, 0])
    test_x = test_x.reshape(1, 48, 44, 8)
    print(time.time() - timer)
    # Loading hex_model
    print("Loading Hexagon Model \n")
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Restrict TensorFlow to only use the fourth GPU
            tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus),
                  "Logical GPUs")

        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)

    hex_model = tf.keras.models.load_model('model/hex_cnn_model_res9')

    # Predicting Parking Vacancy
    h3_pred_y = hex_model.predict(test_x)

    rounded_pred_y = np.round(h3_pred_y)
    prob_pred = estimate_empty_parking_prob(rounded_pred_y, h3_total_capacity)

    # Converting to 2d array to DataFrame
    pred = pd.DataFrame()
    id_value = dict((h3_id_matrix_2d[i][j], prob_pred[i][j]) for i in
                    range(len(h3_id_matrix_2d)) for j in
                    range(len(h3_id_matrix_2d[0])))
    dropped_id_value = {k: v for k, v in id_value.items() if k != ''}
    pred['hex_id'] = dropped_id_value.keys()
    pred['value'] = dropped_id_value.values()
    pred['timestamp'] = rounded_user_time

    final_pred = pred[pred['hex_id'].isin(loc_h3_ring)].sort_values(
        by='value', ascending=False)
    final_pred['lat_lon'] = final_pred['hex_id'].apply(
        lambda x: h3.h3_to_geo(str(x)))
    final_pred['hex_area'] = final_pred['hex_id'].apply(
        lambda x: h3.cell_area(str(x), unit='m^2'))
    const = 2 / 3 ** (3 / 2)
    final_pred['hex_edge'] = final_pred['hex_area'].apply(
        lambda x: (const * x) ** (1 / 2))
    final_pred['lat_lon'] = final_pred['hex_id'].apply(
        lambda x: h3.h3_to_geo(str(x)))

    for n, col in enumerate(['Latitude', 'Longitude']):
        final_pred[col] = final_pred['lat_lon'].apply(
            lambda location: location[n])

    fin_json = final_pred[
        ['value', 'Latitude', 'Longitude', 'hex_edge']].reset_index().drop(
        columns=['index'], axis=1).to_json()
    print("Converting the results into json_file as test_case_results.json")
    parsed = json.loads(fin_json)
    # with open("test_case_results.json", "w") as f:
    #     json.dump(parsed, f)
    print(time.time() - timer)
    return parsed


def hex_cnn(address):
    mygeo = Geocoder(os.environ.get("GEOCODER_API_KEY"))
    loc_lat, loc_lng = mygeo.geocode(address).coordinates
    str_user_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    hex_dict = run_model_with_api(loc_lat, loc_lng, str_user_time)
    return hex_dict

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='sample command: python hex_cnn.py "415 Mission St, San Francisco, CA 94105"')
    parser.add_argument("user_address",
                        help="What's your current address in SF?")
    args = parser.parse_args()
    hex_cnn(args.user_address)
