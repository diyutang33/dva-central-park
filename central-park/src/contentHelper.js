import React from "react";
import {hexColors} from "./staticData";
import {EuiButtonIcon} from "@elastic/eui";
import navigate from './assets/navigate.svg'


export const calcColor = (val) => {
  if (val < 0) return hexColors[0]
  else return hexColors[Math.floor((val * 100) / 10)]
}
export const getNavUrl = (origin, dest) => {
  return `https://www.google.com/maps/dir/${origin.lat},${origin.lng}/${dest.lat},${dest.lng}`
}
const calcCo2 = (dist) => ((dist * 404)/1000).toFixed(2)

export const markerContent = (origin, d, i) =>
  `<div style="text-align: center;">
    <div><b>Destination ${i + 1}</b></div>
    <div>${d.address}</div>
    <div><a href=${getNavUrl(origin, d)} target="_blank">Directions</a></div>
    </div>`


export const parkingListItemLabel = (origin, h, i) => (
  <>
    <span>Availability: {(h.value * 100).toFixed(2)}%</span>
    <br></br>
    <span>Driving from origin: {h.drive_dist} mile | {h.drive_time} min
    </span><br></br>
    <span>Walking to first dest.: {h.walk_dist} mile | {h.walk_time} min</span>
    <br></br>
    <span>Total walking trip: {h.total_walk_dist} mile | {h.total_walk_time} min
    </span><br></br>
    <span>Carbon emissions: {calcCo2(h.drive_dist)} kg of CO<sub>2</sub></span>
  </>
)

export const parkingListItemContent = (origin, h, i) =>
  `<div><b>Hex_${i}</b></div>
    <div>Availability: ${(h.value * 100).toFixed(2)}%</div>
    <div>Driving from origin: ${h.drive_dist} mile | ${h.drive_time} min
    </div>
    <div>Walking to first dest.: ${h.walk_dist} mile | ${h.walk_time} min</div>
    <div>Total walking trip: ${h.total_walk_dist} mile | ${h.total_walk_time} min
    </div>
    <div>Carbon emissions: ${calcCo2(h.drive_dist)} kg of CO<sub>2</sub></div>
    <div><a href=${getNavUrl(origin, h)} target="_blank">Directions</a></div>
`
