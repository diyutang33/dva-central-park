import React from 'react';
import './App.scss';
import mainpage1 from './assets/mainpage1.svg'
import logo from './assets/logo.png'
import MainForm from "./MainForm";

function MainPage(props) {
  return (
    <div id="mainpage">
      <div id="left-half">
        <div id="app-name" className="flex">
          <div id="logo"><img src={logo} alt="logo"/></div>
          <div style={{paddingLeft: "20px"}}>CENTRAL<br/>PARK</div>
        </div>
        <div id="left-half-img"><img src={mainpage1} alt="main"/></div>
        <br></br>
        <div>A CSE6242 project by Team #14: Angela Ahn, Philip Lee, Di Yu Tang, Kris Seo
</div>
      </div>
      <MainForm {...props}/>
    </div>
  );
}

export default MainPage;
