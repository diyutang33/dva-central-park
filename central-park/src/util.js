// dynamically load JavaScript files in our html with callback when finished
import {Loader} from "@googlemaps/js-api-loader";

export function loadScript(url, callback) {
  let script = document.createElement("script"); // create script tag
  script.type = "text/javascript";
  script.async = true;

  // when script state is ready and loaded or complete we will call callback
  if (script.readyState) {
    script.onreadystatechange = function () {
      if (script.readyState === "loaded" || script.readyState === "complete") {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {
    script.onload = () => callback();
  }

  // load by url
  script.src = url;
  // append to head
  // document.getElementsByTagName("head")[0].appendChild(script);
  document.body.appendChild(script);
};


export function getFirstElementByName(name) {
  var elements = document.getElementsByName(name);
  if (elements.length) {
    return elements[0];
  } else {
    return undefined;
  }
}

export function initMapApi(type = "default", callback) {
  let loader;
  if (!window.hasOwnProperty("google")) {
    // TODO: restrict domain
    const placesApiKey = "AIzaSyDbbt1DKjxzeBELFgfzlutHsM4brhf3EjE"
    let params = {
      apiKey: placesApiKey,
      version: "weekly",
    }
    if (type === "places") {
      params["libraries"] = ["places"]
    }
    loader = new Loader(params);
    loader.load().then(callback);
  } else callback()
}