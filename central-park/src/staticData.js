export const walkTimeHelpText = "This is the walking time to the first " +
  "destination. Default is 15 minutes."

export const prefillValues = {
  origin: {
    name: "Caltrain Station, San Francisco",
    address: "700 4th St, San Francisco, CA 94107",
    lat: 37.77667109999999,
    lng: -122.3948431,
  },
  dest: [
    {
      name: "2340 Polk St",
      address: "2340 Polk St, San Francisco, CA 94109",
      lat: 37.79847050000001,
      lng: -122.4220859,
    },
    {
      name: "2360 Fillmore St",
      address: "2360 Fillmore St, San Francisco, CA 94115",
      lat: 37.79150719999999,
      lng: -122.4342211,
    },
    {
      name: "Alta Plaza Park",
      address: "Jackson St. &, Steiner St, San Francisco, CA 94115",
      lat: 37.79120899999999,
      lng: -122.4377047,
    },
    {
      name: "2001 Fillmore St",
      address: "2001 Fillmore St, San Francisco, CA 94115",
      lat: 37.7880739,
      lng: -122.4339571,
    },
  ]
}

export const placesApiFields = [
  "name", "address_component", "formatted_address", "geometry"
]

export const destParkingToggleObj = [
  {
    id: `parkingTabBtn`,
    label: 'Parking Spots',
  },
  {
    id: `destTabBtn`,
    label: 'Destinations',
  },
];

export const hexColors = [
  '#d93951',
  '#ed6a57',
  '#f68f5a',
  '#fbb268',
  '#fed47f',
  '#dde086',
  '#bbea8d',
  '#76cd5a',
  '#49b827',
  '#38921d',
  '#38921d'
]