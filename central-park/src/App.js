import React, {useEffect, useState} from 'react';
import './App.scss';
import Mainpage from "./MainPage"
import Map from "./Map";
import {initMapApi} from "./util";
import {addressObj} from "./models"
import { EuiLoadingLogo } from '@elastic/eui';

function App() {

  const [page, setPage] = useState("main")
  const [mapData, setMapData] = useState(
    {origin: addressObj, dest: [addressObj]})

  const changePage = (page) => {
    setPage(page);
  }

  const getPage = () => {
    if (page === "main") return <Mainpage changePage={changePage}
                                          setMapData={setMapData}/>
    else if (page === "loading") return (
      <div id="loading-page">
        <EuiLoadingLogo logo="gisApp" size="xl" />
        <div>Loading...</div>
        <div>This may take a minute.</div>
      </div>
    )
    else if (page === "map") return <Map mapData={mapData}
                                         changePage={changePage}/>
  }
  useEffect(() => {
    initMapApi("places")
  }, []);
  return (
    <div id="app" className="App">
      {getPage()}
    </div>
  );
}

export default App;
