import React, {useState, useRef, useEffect} from 'react'
import {
  EuiFlexItem, EuiFlexGroup, EuiForm, EuiFieldText, EuiFormRow, EuiButton
} from "@elastic/eui";
import {getFirstElementByName, initMapApi} from "./util"
import DestField from "./DestField";
import {addressObj} from "./models"

import {
  walkTimeHelpText, prefillValues, placesApiFields
} from "./staticData";
// states do not update in event listener, so we use a global variable
let placesObjList = {origin: null, dest: []}

const MainForm = ({changePage, setMapData}) => {
  const [addressList, setAddressList] = useState({
    origin: addressObj, dest: [addressObj]
  })
  const [prefillLoaded, setPrefillLoaded] = useState(false)
  // ref is needed to keep state updated for event listeners
  const addressListRef = useRef(addressList);
  // update both ref and state with one function
  const setAddressListRef = x => {
    addressListRef.current = x;
    setAddressList(x);
  };

  // Used in onChange and handleAddressSelect()
  const handleChange = (type, i, address) => {
    let newAddressObj = {}
    // handling simple field input
    if (typeof address === "string") {
      newAddressObj = {...addressObj, address: address,}
    } else {
      newAddressObj = {
        name: address.name,
        address: address.formatted_address.substring(0, address.formatted_address.length - 5),
        lat: address.geometry.location.lat(),
        lng: address.geometry.location.lng()
      }
    }
    // use ref here because it's updated every render
    const newAddressList = {...addressListRef.current};
    if (type === "origin") newAddressList[type] = newAddressObj;
    else if (type === "dest") newAddressList[type][i] = newAddressObj;
    setAddressListRef(newAddressList);
  }

  const handleAddressSelect = async (type, i) => {
    let addressObject;
    let obj = placesObjList[type]
    if (type === "origin") addressObject = obj.getPlace();
    else if (type === "dest") addressObject = obj[i].getPlace();
    handleChange(type, i, addressObject)
  }

  const initPlacesField = (type, i) => {
    let newplacesObj;
    let sfBound = new window.google.maps.Circle({
      center: new window.google.maps.LatLng(37.76290, -122.44687), radius: 8000
    })
    let options = {
      componentRestrictions: {country: "us"},
      bounds: sfBound.getBounds(),
      strictbounds: true
    }
    let fieldName = type
    if (type === "dest") fieldName += `-${i}`
    newplacesObj = new window.google.maps.places.Autocomplete(
      getFirstElementByName(fieldName), options
    );
    newplacesObj.setFields(placesApiFields);
    newplacesObj.addListener("place_changed", () =>
      handleAddressSelect(type, i)
    );
    if (type === "origin") placesObjList[type] = newplacesObj;
    else if (type === "dest") placesObjList[type][i] = newplacesObj;
  }

  const addDest = () => {
    setAddressListRef({
      ...addressList, dest: [...addressList.dest, addressObj]
    })
  }

  const removeDest = (i) => {
    let newDestList = [...addressList.dest];
    newDestList.splice(i, 1);
    setAddressListRef({...addressList, dest: newDestList});
    placesObjList.dest.splice(i, 1);
  }

  const prefillFields = () => {
    setPrefillLoaded(true);
    setAddressListRef(prefillValues);
  }

  const renderPrefillBtn = () => {
    if (window.location.pathname == "/demo"
      || window.location.pathname == "/mock") {
      return (<EuiFlexItem>
        <EuiButton fullWidth onClick={() => prefillFields()}>Prefill Fields
        </EuiButton>
      </EuiFlexItem>)
    }
  }

  useEffect(() => {
    if (prefillLoaded) {
      initMapApi("places", () => {
        initPlacesField("dest", 1);
        initPlacesField("dest", 2);
        initPlacesField("dest", 3);
      })
    }
  }, [prefillLoaded])

  // initiated with current time
  const handleSubmit = (event) => {
    event.preventDefault();
    changePage("loading")
    let maxWalkTime = document.getElementsByName("maxWalkTime")[0].value
    if (maxWalkTime == "") maxWalkTime = 15
    let postData = {
      addressList: addressList,
      maxWalkTime: maxWalkTime,
      source: window.location.pathname
    }
    const hexcnn = async () => {
      const response = await fetch('/api/hexcnn', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(postData)
      }).catch((error) => {
        console.log(error)
      });
      const data = await response.json();
      return data
    }
    hexcnn().then(data => {
      console.log(data)
      setMapData(data)
      changePage("map");
    });
  }

  useEffect(() => {
    initMapApi("places", () => {
      initPlacesField("origin", null);
      initPlacesField("dest", 0);
    })
  }, [])

  useEffect(() => {
    initMapApi("places", () => {
      initPlacesField("dest", addressList.dest.length - 1);
    })
  }, [addressList.dest.length]);

  return (
    <EuiForm component="form" onSubmit={handleSubmit}>
      <EuiFormRow label="Origin">
        <EuiFieldText
          name="origin" icon="annotation" placeholder="Address"
          value={addressList.origin.address} onChange={
          e => handleChange("origin", null, e.target.value)}/>
      </EuiFormRow>
      <DestField addressList={addressList} removeDest={removeDest}
                 addDest={addDest} handleChange={handleChange}/>
      <EuiFormRow label="Maximum Walking Time" helpText={walkTimeHelpText}>
        <EuiFieldText name="maxWalkTime" icon="clock" placeholder="Minutes"/>
      </EuiFormRow>
      <EuiFormRow>
        <EuiFlexGroup gutterSize="s" wrap>
          {renderPrefillBtn()}
          <EuiFlexItem>
            <EuiButton type="submit" fullWidth fill>Go!</EuiButton>
          </EuiFlexItem>
        </EuiFlexGroup>
      </EuiFormRow>
    </EuiForm>
  )
}

export default MainForm