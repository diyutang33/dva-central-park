import React from 'react'
import {
  EuiFieldText, EuiFormRow, EuiFormLabel,
  EuiButtonIcon, EuiButton,
} from "@elastic/eui";

const DestField = ({addressList, addDest, removeDest, handleChange}) => {
  const destHelpText = "Walking time and distance will be calculated " +
    "from the parking spot to the first destination if multiple are specified."
  const multiDest = () => addressList.dest.length > 1;

  return (
    <>
      {addressList.dest.map((element, i) => (
        <EuiFormRow key={i} label={!i ? "Destinations" : null}
                    helpText={!i ? destHelpText : null}>
          <EuiFieldText
            name={"dest-" + i}
            prepend={multiDest() ?
              <EuiFormLabel>{i + 1}</EuiFormLabel> : null}
            icon="visMapCoordinate"
            placeholder="Address"
            value={element.address}
            onChange={e => handleChange("dest", i, e.target.value)}
            append={multiDest() ?
              <EuiButtonIcon iconType="cross" aria-label="delete-destination"
                             onClick={() => removeDest(i)}/> : null}
            aria-label="destination"
          />
        </EuiFormRow>

      ))}
      <EuiFormRow className="flex-right">
        <EuiButton iconType="plus" onClick={() => addDest()}>
          Add Destination
        </EuiButton>
      </EuiFormRow>
    </>
  )
}

export default DestField