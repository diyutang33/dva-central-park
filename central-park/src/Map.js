import React, {useState, useEffect} from 'react'
import {initMapApi} from "./util";
import "./map.scss"
import {
  EuiCard, EuiSpacer, EuiBetaBadge, EuiButtonGroup, EuiIcon, EuiPanel,
  EuiSwitch, EuiRange
} from "@elastic/eui";
import vertical_dots from './assets/vertical_dots.svg'
import {destParkingToggleObj} from "./staticData";
import {
  calcColor, markerContent, parkingListItemLabel, parkingListItemContent,
  getNavUrl
} from "./contentHelper"
import navigate from "./assets/navigate.svg";

let polygonList = []
let destCardList = []

const Map = ({mapData, changePage}) => {
  const [toggleSelected, setToggleSelected] = useState("parkingTabBtn");
  const [showCloseHex, setShowCloseHex] = useState(true);
  const [maxWalkTime, setmaxWalkTime] = useState(mapData.maxWalkTime);

  const initMap = (mapData) => {
    let firstDest = mapData.dest[0]
    let destPos = new window.google.maps.LatLng(firstDest.lat, firstDest.lng)
    let map = new window.google.maps.Map(
      document.getElementById("map-vis"), {
        center: destPos, zoom: 15, mapId: "9822d7b59fddcc2a"
      });
    drawHexagonGrid(map);
    mapData.dest.forEach((d, i) => {
      setTimeout(() => {
        let newMarker = new window.google.maps.Marker({
          position: {lat: d.lat, lng: d.lng},
          map: map,
          label: {
            text: (i + 1).toString(), color: "white", fontWeight: "bold"
          },
          animation: window.google.maps.Animation.DROP,
        })
        const infoWindow = new window.google.maps.InfoWindow({
          content: markerContent(mapData.origin, d, i),
        });
        newMarker.addListener("click", () => {
          infoWindow.open({
            anchor: newMarker, map: map, shouldFocus: true,
          });
        })
        destCardList.push(newMarker);
      }, i * 500);
    })
    map.setCenter(destPos);
  }

  const calcOffset = (position, width, degree) => {
    return window.google.maps.geometry.spherical.computeOffset(
      position, width, degree)
  }

  function drawHexagonGrid(map) {
    let destPos;
    mapData.hex.forEach((h, i) => {
      destPos = new window.google.maps.LatLng(h.lat, h.lng)
      drawHexagon(map, destPos, h, i)
    })
  }

  function drawHexagon(map, position, hex, i) {
    let coordinates = [];
    for (let angle = 85; angle < 445; angle += 60) {
      coordinates.push(calcOffset(position, hex.hexEdge, angle));
    }
    let fillColor = calcColor(hex.value)
    let polygon = new window.google.maps.Polygon({
      paths: coordinates,
      strokeColor: fillColor,
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: fillColor,
      fillOpacity: 0.5
    });
    const infoWindow = new window.google.maps.InfoWindow({
      content: parkingListItemContent(mapData.origin, hex, i),
    });
    polygon.setMap(map);
    polygon.addListener("click", () => {
      infoWindow.setPosition({lat: hex.lat, lng: hex.lng});
      infoWindow.open({map, shouldFocus: true});
    })
    polygonList.push(polygon)
  }

  function formatAddress(address) {
    let splitIndex = address.indexOf("San Francisco");
    let streetAddress = address.substring(0, splitIndex - 1);
    let restAddress = address.substring(splitIndex, address.length)
    return (<><span>{streetAddress}</span><br></br><span>{restAddress}</span>
    </>)
  }

  useEffect(() => {
    initMapApi("default", () => initMap(mapData));
  }, [mapData]);


  useEffect(() => {
    mapData.hex.forEach((h, i) => {
      if (showCloseHex && h.walk_time > maxWalkTime) {
        polygonList[i].setVisible(false)
      } else if (!showCloseHex || h.walk_time <= maxWalkTime) {
        polygonList[i].setVisible(true)
      }
    })
  }, [showCloseHex, maxWalkTime]);

  const destListClick = (i) => {
    window.google.maps.event.trigger(destCardList[i], 'click');
  }

  const parkingListClick = (i) => {
    window.google.maps.event.trigger(polygonList[i], 'click');
  }

  const parkingList = () => {
    return (<EuiPanel id="parking-hex-list" paddingSize="s" color="plain">
      {mapData.hex.map((h, i) => (
        <span key={i}>
          <EuiCard
            className="parking-hex-list-card" layout="horizontal"
            paddingSize="s"
            icon={
              <div className="node-icon">
                <EuiIcon type="node" size="l" color={calcColor(h.value)}/>
              </div>}
            title={<div className="parking-hex-list-header">
              <div className="header-text" style={{color: calcColor(h.value)}}>
                Hex_{i}
              </div>
              <a href={getNavUrl(origin, h)} target="_blank">
                <img height="20px" src={navigate} alt="navigate"/>
              </a>
            </div>}
            description={parkingListItemLabel(mapData.origin, h, i)}
            onClick={() => parkingListClick(i)}
          />
          <EuiSpacer size="s"/>
        </span>))}
    </EuiPanel>)
  }

  const destList = () => (
    <EuiPanel id="parking-hex-list" paddingSize="s" color="plain">
      <EuiCard
        layout="horizontal" titleSize="xs"
        icon={<EuiBetaBadge size="s" label="Origin" color="accent"/>}
        title={mapData.origin.name}
        description={formatAddress(mapData.origin.address)}/>
      <EuiSpacer size="s"/>
      <div style={{"textAlign": "center", "padding": "5px 0px"}}>
        <img height="30px" src={vertical_dots} ailt="vertical dots"/>
      </div>
      <EuiSpacer size="s"/>
      {mapData.dest.map((d, i) => (
        <div key={i}>
          <EuiCard layout="horizontal" titleSize="xs" title={d.name}
                   description={formatAddress(d.address)}
                   icon={<EuiBetaBadge color="accent" size="s"
                                       label={"DEST " + (i + 1)}/>}
                   onClick={() => {
                     destListClick(i)
                   }}/>
          <EuiSpacer size="s"/>
        </div>
      ))}
    </EuiPanel>
  )

  const maxWalkTimeToggle = () => {
    let label = (<span>Hiding all hexagons</span>)
    if (maxWalkTime > 0) {
      label = (<span>Show hexagons &#8804; {maxWalkTime} walking minutes away
        from the first destination</span>)
    }
    return (<>
      <EuiSwitch
        checked={showCloseHex} label={label}
        onChange={() => setShowCloseHex(!showCloseHex)}/>
      <EuiRange
        showTicks min={0} max={30} step={1} value={maxWalkTime}
        onChange={e => setmaxWalkTime(e.target.value)}
        ticks={[
          {label: 'Hide all', value: 0},
          {label: '30 minutes', value: 30}]}/>
    </>)
  }

  return (
    <div id="map">
      <div id="map-list">
        <EuiButtonGroup legend="Parking Destination Toggle Group"
                        buttonSize="m" isFullWidth idSelected={toggleSelected}
                        options={destParkingToggleObj} color="primary"
                        onChange={(id) => setToggleSelected(id)}/>
        <EuiSpacer size="s"/>
        {toggleSelected === "destTabBtn" ? destList() : parkingList()}
        <EuiSpacer size="s"/>
        <EuiPanel id="map-settings" paddingSize="s" color="plain">
          {maxWalkTimeToggle()}
        </EuiPanel>
      </div>
      <div id="map-vis"></div>
    </div>
  )
}

export default Map