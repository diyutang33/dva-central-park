# Central Park

Fall 2021 CSE6242

Team #14: Angela Ahn, Philip Lee, Di Yu Tang, Kris Seo

https://gitlab.com/diyutang33/dva-central-park

A web application that utilizes Hexagonal Convolutional Neural Networks to predict parking availability based on user selected destinations, preferences, and historical parking transaction data.

This application has been published to http://centralparkapp.com/. http://centralparkapp.com/demo contains a button that pre-fills fields for an easy demo.

## Tech Stack

**Client:** React, Elastic UI

**Server:** Flask, Nginx, Gunicorn


## DESCRIPTION


`/central-park` contains all necessary code for building and serving the web application. Aside from the subdirectories below, this directory also contains `package.json` which lists frontend dependencies and metadata used by NPM. `requirements.txt` lists Python dependencies.
* `/central-park/api` is the self-contained Flask backend application written in Python.
  * `/central-park/api/model` contains the trained square and hex CNN models. The square model is only used for benchmarking and is not used in the application.
  * `/central-park/api/pickle_data` folder contains additional parking data formatted as Python byte stream which will be read when running the HexCNN model.
  * `/central-park/api/api.py` is the entry point for Flask, and route decorated functions that handles client requests and will begin the backend processing pipeline.
* `/central-park/public` contains static files for the frontend client.
  * `/central-park/public/index.html` is the entry point the React library.
* `/central-park/src` contains all frontend SCSS files, React components, JavaScript helper functions, and images.
* `/central-park/data` contains training and test data and trained models.
* `/central-park/jupter-notebook` contains Jupyter notebooks serving as playgrounds for processing San Francisco parking data, training a CNN models, testing, and validation.

## INSTALLATION

The following instructions are verified for MacOS and Linux based systems.

Download and install Python 3.7 or 3.8 from https://www.python.org/downloads/. This application has been verified to work with these versions. We will be using Python 3.7 for the remainder of this guide. After installing Python, open the terminal and run `which python3.7` to make sure that it's installed.

Start by going into the `/central-park` directory:
```bash
$ cd central-park
```

Create virtual environment and activate it. We will use `centralpark-env` as the environment name.
```bash
$ python3.7 -m venv centralpark-env
$ source centralpark-env/bin/activate
```

The environment name should now be prepended to terminal line like:
```bash
(centralpark-env) $ _
```

Install all Python packages. Make sure you are still in the `/central-park` directory:
```bash
(centralpark-env) $ pip install -r "requirements.txt"
```

Install Node.js via https://nodejs.org/en/download/ then install Yarn with:
```bash
(centralpark-env) $ npm install --global yarn
```

Install node packages:
```bash
(centralpark-env) $ yarn install
```

## EXECUTION
Within the `/central-park/api` directory, if the `.flaskenv` file is not there,
create one by running
```bash
(centralpark-env) $ touch .faskenv
```

Register accounts and get API keys from Socrata, Geocoder, and Bing Maps, then fill `.flaskenv` with the following format and replace `#` the relevant information.

```
FLASK_APP=api.py
FLASK_ENV=development
SOCRATA_API_KEY=#
SOCRATA_API_USERNAME=#@#.com
SOCRATA_API_PASSWORD=#
GEOCODER_API_KEY=#
BING_ROUTES_API_KEY=#
```

This project requires that the React frontend server and Flask backend server to be run simultaneously.

To start the backend server, first make sure the Python virtual environment is activated.
Then within the `/central-park/api` directory, run:
```bash
(centralpark-env) $ export PYTHONPATH=.
(centralpark-env) $ flask run
```

To start the frontend server, open a second terminal and run:
```bash
(centralpark-env) $ yarn start
```

The website should now be available at `http://localhost:3000`
